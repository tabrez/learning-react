import React, { createContext } from "react";
import ReactDOM from "react-dom";
import App from "./app.js";

import "react-grid-layout/css/styles.css";
import "react-grid-layout/node_modules/react-resizable/css/styles.css";

ReactDOM.render(<App />, document.getElementById("root"));
