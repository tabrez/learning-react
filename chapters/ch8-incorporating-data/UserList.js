import React from "react";
import { FixedSizeList } from "react-window";

const UserList = ({ data }) => {
  const RenderRow = ({ index, style }) => {
    return (
      <div style={{ ...style, ...{ display: "flex" } }}>
        <p>
          {data[index].name} - {data[index].email}
        </p>
      </div>
    );
  };

  return (
    <FixedSizeList
      height={200}
      width={window.innerWidth - 20}
      itemCount={data.length}
      itemSize={30}
    >
      {RenderRow}
    </FixedSizeList>
  );
};

export default UserList;
