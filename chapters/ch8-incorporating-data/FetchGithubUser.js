import React from "react";
import useFetch from "./UseFetch.js";

const loadJSON = (key) => {
  return key && JSON.parse(localStorage.getItem(key));
};

const saveJSON = (key, data) => {
  localStorage.setItem(key, JSON.stringify(data));
};

const Fetch = ({
  uri,
  renderSuccess,
  loadingFallback = <p>loading...</p>,
  renderError = (error) => <pre>{JSON.stringify(error, null, 2)}</pre>,
}) => {
  const { loading, data, error } = useFetch(uri);
  if (loading) return loadingFallback;
  if (error) return renderError(error);
  if (data) return renderSuccess({ data });
};

const UserDetails = ({ data }) => {
  return (
    <div className="githubuser">
      <img src={data.avatar_url} alt={data.login} style={{ width: 200 }} />
      <div>
        <h1>{data.login}</h1>
        {data.message && <p>{data.message}</p>}
        {data.name && <p>{data.name}</p>}
        {data.location && <p>{data.location}</p>}
      </div>
    </div>
  );
};

const GitHubUser = ({ login }) => {
  return (
    <div>
      <Fetch
        uri={`https://api.github.com/users/${login}`}
        renderSuccess={UserDetails}
      />
      <Fetch
        uri={`https://api.github.com/users/tabrez`}
        loadingFallback={<p>Loading spinner here</p>}
        renderError={(error) => {
          return <p>Something went wrong...{error.message}</p>;
        }}
        renderSuccess={(data) => (
          <div>
            <h1>Github user details</h1>
            <pre>{JSON.stringify(data, null, 2)}</pre>
          </div>
        )}
      />
    </div>
  );
  // if (!data) return null;
  // if ("message" in data && data.message === "Not Found") setError(data);
};

export { loadJSON, saveJSON };
export default GitHubUser;
