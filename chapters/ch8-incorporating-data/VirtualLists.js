import React from "react";

const Peaks = ({ data, renderItem, renderEmpty }) => {
  if (!data.length) return renderEmpty;
  return (
    <ul>
      {<p>List of peaks:</p>}
      {data.map((peak, i) => {
        return <li key={i}>{renderItem(peak)}</li>;
      })}
    </ul>
  );
};

export default Peaks;
