import React, { useState } from "react";
import GitHubUser from "./FetchGithubUser.js";
import Peaks from "./VirtualLists.js";
import UserList from "./UserList.js";
import faker from "faker";

const tahoe_peaks = [
  { name: "Freel Peak", elevation: 10891 },
  { name: "Monument Peak", elevation: 10067 },
  { name: "Pyramid Peak", elevation: 9983 },
  { name: "Mt. Tallac", elevation: 9735 },
];

const users = [...Array(5000)].map(() => ({
  name: faker.name.findName(),
  email: faker.internet.email(),
}));

const App = () => {
  const [login, setLogin] = useState("moontahoe");
  return (
    <div>
      <input
        type="text"
        value={login}
        label="Github login name"
        onChange={(e) => setLogin(e.target.value)}
      />
      <GitHubUser login={login} />
      <Peaks data={[]} renderEmpty={<p>List of peaks found empty</p>} />
      <Peaks
        data={tahoe_peaks}
        renderEmpty={<p>List of peaks found empty</p>}
        renderItem={(item) => {
          return (
            <div>
              {item.name} --- {item.elevation.toLocaleString()}ft
            </div>
          );
        }}
      />
      <UserList data={users} />
    </div>
  );
};

export default App;
