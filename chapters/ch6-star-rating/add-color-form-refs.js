import React, { useRef } from "react";

const AddColorForm = ({ onNewColor = (x) => x }) => {
  const txtTitle = useRef();
  const hexColor = useRef();

  const submit = (e) => {
    e.preventDefault();
    onNewColor(txtTitle.current.value);
    onNewColor(hexColor.current.value);
    txtTitle.current.value = "";
    hexColor.current.value = "";
  };

  return (
    <form onSubmit={submit}>
      <input ref={txtTitle} type="text" placeholder="color title..." required />
      <input ref={hexColor} type="color" required />
      <button>ADD</button>
    </form>
  );
};

export default AddColorForm;
