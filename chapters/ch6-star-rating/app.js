import React, { useState } from "react";
import colorData from "./color-data.json";
import ColorList from "./color-list.js";
import AddColorForm from "./add-color-form-input-hook.js";

const App = () => {
  return (
    <div>
      <AddColorForm />
      <ColorList />
    </div>
  );
};

export default App;
