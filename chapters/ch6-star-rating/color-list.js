import React, { useContext } from "react";
import Color from "./color.js";
import { ColorContext } from "./color-provider";

const ColorList = () => {
  const { colors, rateColor } = useContext(ColorContext);
  if (!colors.length) return <div>No colors listed.</div>;
  return (
    <div>
      {colors.map((color) => (
        <Color key={color.id} {...color} onRate={rateColor} />
      ))}
    </div>
  );
};

export default ColorList;
