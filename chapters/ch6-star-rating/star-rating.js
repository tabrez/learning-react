import React, { useState } from "react";
import Star from "./star.js";

const StarRating = ({
  style = {},
  totalStars = 5,
  selectedStars = 0,
  onRate = (x) => x,
  props = {},
}) => {
  return (
    <div style={{ padding: "5px", ...style, ...props }}>
      {[...Array(totalStars).keys()].map((i) => (
        <Star
          key={i}
          selected={selectedStars > i}
          onSelect={() => onRate(i + 1)}
        />
      ))}
    </div>
  );
};

export default StarRating;
