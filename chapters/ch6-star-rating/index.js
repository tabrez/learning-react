import React, { createContext } from "react";
import ReactDOM from "react-dom";
import App from "./app.js";
import { ColorProvider } from "./color-provider.js";

ReactDOM.render(
  <ColorProvider>
    <App />
  </ColorProvider>,
  document.getElementById("root")
);
