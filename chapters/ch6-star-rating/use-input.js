import { useState } from "react";

const useInput = (init) => {
  const [value, setValue] = useState(init);
  return [
    { value, onChange: (e) => setValue(e.target.value) },
    () => setValue(init),
  ];
};

export default useInput;
