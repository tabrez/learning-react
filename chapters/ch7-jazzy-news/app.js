import React, { useState, useEffect } from "react";

const App = () => {
  const [val, setVal] = useState("");
  const [phrase, setPhrase] = useState("example phrase");

  const createPhrase = () => {
    setPhrase(val);
    setVal("");
  };

  useEffect(() => {
    console.log("called only once after initial render");
  }, []);

  useEffect(() => {
    console.log(`typing "${val}"`);
  }, [val]);

  useEffect(() => {
    console.log(`saved phrase: "${phrase}"`);
  }, [phrase]);
  return (
    <div>
      <label>Favourite phrase:</label>
      <input
        value={val}
        placeholder={phrase}
        onChange={(e) => setVal(e.target.value)}
      />
      <button onClick={createPhrase}>send</button>
    </div>
  );
};

export default App;
