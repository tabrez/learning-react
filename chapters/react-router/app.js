import React from "react";
import { Route, Switch } from "react-router-dom";
import { Home, About, Events, Products, Contact, NotFound404 } from "./pages";

import { BrowserRouter as Router } from "react-router-dom";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/events">
          <Events />
        </Route>
        <Route path="/products">
          <Products />
        </Route>
        <Route path="/contact">
          <Contact />
        </Route>
        <Route path="/">
          <Home />
        </Route>
        <Route path="*">
          <NotFound404 />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
