import React from "react";

import { Link, Outlet } from "react-router-dom";
export const Home = () => {
  return (
    <div>
      <h1>[Company website]</h1>
      <nav>
        <Link to="/">Home</Link> <br />
        <Link to="/about">About</Link> <br />
        <Link to="/events">Events</Link> <br />
        <Link to="/products">Products</Link> <br />
        <Link to="/contact">Contact</Link> <br />
      </nav>
    </div>
  );
};

export const About = () => {
  return (
    <div>
      <h1>[About]</h1>
      <Outlet />
    </div>
  );
};

export const Events = () => {
  return (
    <div>
      <h1>[Events]</h1>
    </div>
  );
};

export const Products = () => {
  return (
    <div>
      <h1>[Products]</h1>
    </div>
  );
};

export const Contact = () => {
  return (
    <div>
      <h1>[Contact]</h1>
    </div>
  );
};

export const NotFound404 = () => {
  return (
    <div>
      <h1>Resource not found</h1>
    </div>
  );
};

export const History = () => {
  <section>
    <h2>Our Services</h2>
    <p>
      Hello world <br /> This is our history
    </p>
  </section>;
};

export const Services = () => {
  <section>
    <h2>Our Services</h2>
    <p>
      Hello world <br /> These are our services
    </p>
  </section>;
};

export const Location = () => {
  <section>
    <h2>Our Services</h2>
    <p>
      Hello world <br /> These are our locations
    </p>
  </section>;
};
